$(document).ready(function (){		// when document is ready
	var numboxes = prompt("How many boxes?", "500");

	var i = 0;
	while (i < numboxes) {
		var x = $("<div class='box'></div>");
		$("body").append(x);
		i ++;
	}

	$(".box").on("mouseover", function (e) {
		$(this).css("background-color", "blue");
	});

	$(".box").on("mouseleave", function (e) {
		$(this).css("background-color", "white");
	});

});
