/**
 * Created by adude on 01/03/2016.
 */

$(document).ready(function (){
    $("#first").on('input', function () {
        reflect();
    });

    $("#check").on('click', function () {
        reflect();
    });
});


function reflect () {
    if ($("#check").prop('checked')) {
        $("#second").val($("#first").val());
    }
}